# git
[toc] 

## 什么是git  
![top](resource\git-gti.png)

简介：
: * Git 是一个开源的分布式版本控制系统，用于敏捷高效地处理任何或小或大的项目。

: * Git 是 Linus Torvalds 为了帮助管理 Linux 内核开发而开发的一个开放源码的版本控制软件。

: * Git 与常用的版本控制工具 CVS, Subversion 等不同，它采用了分布式版本库的方式，不必服务器端软件支持。

![process](resource\git-process.png)

------
![working](resource\git-space.jpg)
### Git工作区、暂存区和版本库
工作区、暂存区和版本库概念：
: * 工作区：就是你在电脑里能看到的目录。
: * 暂存区：英文叫 stage 或 index。一般存放在 .git 目录下的 index 文件（.git/index）中，所以我们把暂存区有时也叫作索引（index）。
: * 版本库：工作区有一个隐藏目录 .git，这个不算工作区，而是 Git 的版本库。

图中左侧为工作区，右侧为版本库。在版本库中标记为 "index" 的区域是暂存区（stage/index），标记为 "master" 的是 master 分支所代表的目录树。

图中我们可以看出此时 "HEAD" 实际是指向 master 分支的一个"游标"。所以图示的命令中出现 HEAD 的地方可以用 master 来替换。

图中的 objects 标识的区域为 Git 的对象库，实际位于 ".git/objects" 目录下，里面包含了创建的各种对象及内容。

当对工作区修改（或新增）的文件执行 git add 命令时，暂存区的目录树被更新，同时工作区修改（或新增）的文件内容被写入到对象库中的一个新的对象中，而该对象的ID被记录在暂存区的文件索引中。

当执行提交操作（git commit）时，暂存区的目录树写到版本库（对象库）中，master 分支会做相应的更新。即 master 指向的目录树就是提交时暂存区的目录树。

当执行 git reset HEAD 命令时，暂存区的目录树会被重写，被 master 分支指向的目录树所替换，但是工作区不受影响。

当执行 git rm --cached <file> 命令时，会直接从暂存区删除文件，工作区则不做出改变。

当执行 git checkout . 或者 git checkout -- <file> 命令时，会用暂存区全部或指定的文件替换工作区的文件。这个操作很危险，会清除工作区中未添加到暂存区中的改动。

当执行 git checkout HEAD .  或者 git checkout HEAD <file> 命令时，会用 HEAD 指向的 master 分支中的全部或者部分文件替换暂存区和以及工作区中的文件。这个命令也是极具危险性的，因为不但会清除工作区中未提交的改动，也会清除暂存区中未提交的改动。
### Git 与 SVN 区别
Git 不仅仅是个版本控制系统，它也是个内容管理系统(CMS)，工作管理系统等。

如果你是一个具有使用 SVN 背景的人，你需要做一定的思想转换，来适应 Git 提供的一些概念和特征。

Git 与 SVN 区别点：
![git svn](resource\git-gitsvn.jpg)

1. Git 是分布式的，SVN 不是：这是 Git 和其它非分布式的版本控制系统，例如 SVN，CVS 等，最核心的区别。

2. Git 把内容按元数据方式存储，而 SVN 是按文件：所有的资源控制系统都是把文件的元信息隐藏在一个类似 .svn、.cvs 等的文件夹里。

3. Git 分支和 SVN 的分支不同：分支在 SVN 中一点都不特别，其实它就是版本库中的另外一个目录。

4. Git 没有一个全局的版本号，而 SVN 有：目前为止这是跟 SVN 相比 Git 缺少的最大的一个特征。

5. Git 的内容完整性要优于 SVN：Git 的内容存储使用的是 SHA-1 哈希算法。这能确保代码内容的完整性，确保在遇到磁盘故障和网络问题时降低对版本库的破坏。
## git的基本使用
安装后cmd输入 git --version查看自带的版本

### ！git init
Git 使用 git init 命令来初始化一个 Git 仓库
```shell
#使用当前目录作为 Git 仓库,亦可指定其他地址
git init
#该命令执行完后会在当前目录生成一个 .git 目录
```
### git clone 
我们使用 git clone 从现有 Git 仓库中拷贝项目（类似 svn checkout）
```shell
#执行如下命令以创建一个本地仓库的克隆版本：
git clone /path/to/repository
#如果是远端服务器上的仓库，你的命令会是这个样子：
git clone username@host:/path/to/repository
```
### 用户与邮箱
```shell
#1.查看用户与邮箱信息
git config --global --list
#2.配置用户名与邮箱
git config user.name
git config user.email
#    若需要修改
    git config --global user.name "username"
    git config --global user.email "email"
```

### 提交与修改
|cmd/shell|explain|
|------|------|
|git add |添加文件到暂存区 |
|git status |查看仓库当前的状态，显示有变更的文件|
|git diff | 比较文件的不同，即暂存区和工作区的差异|
|git commit |提交暂存区到本地仓库|
|git reset |回退版本|
|git rm |将文件从暂存区和工作区中删除|
|git mv |移动或重命名工作区文件|

### 提交日志
|cmd/shell |explain|
|---|---|
|git log |	查看历史提交记录 |
|git blame <file> |以列表形式查看指定文件的历史修改记录 |
 ### 远程操作
|cmd/shell |explain |
|---|---|
|git remote |	远程仓库操作 |
|git fetch | 	从远程获取代码库|
|git pull|下载远程代码并合并 |
|git push |上传远程代码并合并 |

### git config --list
```shell

#获取默认配置，如果当前地址中仓库信息不存在，则查看全局，然后再读取系统配置

git config --list
 
#本地仓库配置 高优先级
git config --local --list
 
#全局用户配置 中优先级
git config --global --list
 
#系统配置 低优先级
git config --system --list

 ```
配置文件作用域
: --local 使用本地仓库配置文件进行后续的操作命令
: --global  使用全局配置文件进行后续的操作命令
: --system 使用系统配置文件进行后续的操作命令
: --worktree            use per-worktree config file
: -f, --file <file>     use given config file
: --blob <blob-id>      read config from given blob object

属性操作
: --get  获取指定键的值（根据作用域，只返回第一个匹配到的值）
: --get-all  获取键的所有值（读取所以配置文件的值）
: --get-regexp          get values for regexp: name-regex [value-pattern]
: --get-urlmatch        get value specific for the URL: section[.var] URL
: --replace-all  替换键的值
: --add 添加键值对
: --unset  移除键值对，只有键时，将会删除该键值对，键后跟值时，代表仅删除改值
--unset-all           remove all matches: name [value-pattern]
--rename-section      rename section: old-name new-name
--remove-section      remove a section: name
-l, --list 展示所有参数
--fixed-value         use string equality when comparing values to 'value-pattern'
-e, --edit  打开编辑器
--get-color           find the color configured: slot [default]
--get-colorbool       find the color setting: slot [stdout-is-tty]
属性类型
-t, --type <>         value is given this type
--bool                value is "true" or "false"
--int                 value is decimal number
--bool-or-int         value is --bool or --int
--bool-or-str         value is --bool or string
--path                value is a path (file or directory name)
--expiry-date         value is an expiry date

其他
: -z, --null            terminate values with NUL byte
--name-only           show variable names only
--includes            respect include directives on lookup
--show-origin         show origin of config (file, standard input, blob, command line)
--show-scope          show scope of config (worktree, local, global, system, command)
--default <value>     with --get, use default value when missing entry
